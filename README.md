## Montando o README do projeto

* O <em>README.MD</em> deverá ter as seguintes funções mínimas:
    * No ínicio do README
        * Markdown do SonnarQube
            * Copiar todos os badges, conforme imagem abaixo, e deixar o seu markdown
            no README 
            ![sonnarqube](/uploads/28328dc5325276da662a0e89b75a5549/sonnarqube.png)
            
        * Markdown do Jenkins  
            * Selecionar Embeddable Build Status na paǵina de configuração do pipeline  
                * Copiar o markdown do build do Jenkins
                ![jenkins_badge](/uploads/b0bab0e2b30a3e9a3e8b19321b152fb5/jenkins_badge.png)
    * Cliente do projeto
    * Finalidade do código que está nesse repositório
    * Estrutura de diretório composto no projeto
        * Obs.: Estrutura deverá obedecer esse template

#####################################################################################
#####################################################################################

Exemplo do hospital-care-transformation:

[![Quality Gate Status](http://sonarqube.semantixmanager.com:19000/api/project_badges/measure?project=hospital-care-transformation&metric=alert_status)](http://sonarqube.semantixmanager.com:19000/dashboard?id=hospital-care-transformation)
[![Bugs](http://sonarqube.semantixmanager.com:19000/api/project_badges/measure?project=hospital-care-transformation&metric=bugs)](http://sonarqube.semantixmanager.com:19000/dashboard?id=hospital-care-transformation)
[![Code Smells](http://sonarqube.semantixmanager.com:19000/api/project_badges/measure?project=hospital-care-transformation&metric=code_smells)](http://sonarqube.semantixmanager.com:19000/dashboard?id=hospital-care-transformation)
[![Coverage](http://sonarqube.semantixmanager.com:19000/api/project_badges/measure?project=hospital-care-transformation&metric=coverage)](http://sonarqube.semantixmanager.com:19000/dashboard?id=hospital-care-transformation)
[![Duplicated Lines (%)](http://sonarqube.semantixmanager.com:19000/api/project_badges/measure?project=hospital-care-transformation&metric=duplicated_lines_density)](http://sonarqube.semantixmanager.com:19000/dashboard?id=hospital-care-transformation)
[![Lines of Code](http://sonarqube.semantixmanager.com:19000/api/project_badges/measure?project=hospital-care-transformation&metric=ncloc)](http://sonarqube.semantixmanager.com:19000/dashboard?id=hospital-care-transformation)
[![Maintainability Rating](http://sonarqube.semantixmanager.com:19000/api/project_badges/measure?project=hospital-care-transformation&metric=sqale_rating)](http://sonarqube.semantixmanager.com:19000/dashboard?id=hospital-care-transformation)
[![Reliability Rating](http://sonarqube.semantixmanager.com:19000/api/project_badges/measure?project=hospital-care-transformation&metric=reliability_rating)](http://sonarqube.semantixmanager.com:19000/dashboard?id=hospital-care-transformation)
[![Security Rating](http://sonarqube.semantixmanager.com:19000/api/project_badges/measure?project=hospital-care-transformation&metric=security_rating)](http://sonarqube.semantixmanager.com:19000/dashboard?id=hospital-care-transformation)
[![Techical Debt](http://sonarqube.semantixmanager.com:19000/api/project_badges/measure?project=hospital-care-transformation&metric=sqale_index)](http://sonarqube.semantixmanager.com:19000/dashboard?id=hospital-care-transformation)

[![Quality Gate Status](http://sonarqube.semantixmanager.com:19000/api/project_badges/measure?project=hospital-care-transformation&metric=alert_status)](http://sonarqube.semantixmanager.com:19000/dashboard?id=hospital-care-transformation)

[![Build Status](http://jenkins.semantixmanager.com:8080/buildStatus/icon?job=smtx-hospital-care-transformation)](http://jenkins.semantixmanager.com:8080/job/smtx-hospital-care-transformation/)

# Projeto Hospital Care Transformação

* Versão: 0.1
* Autor: Semantix
* Data: 16/08/2019

## Cliente
- Hospital Care: 

Uma Holding com recursos de dois fundos principais (Crescera e Abaporu) que vem crescendo e comprando grandes hospitais regionais (serviço de saúde terceário) de zonas fora de grandes centros; após ela compra clinicas, laboratórios e seguradoras locais; isso dá origem a um HUB de saúde. Atualmente ela tem 3 HUBs : 
	- Campinas (o maior deles)
	- Florianópolis
	- Ribeirão preto
O objetivo deles é chegar à 12 HUBs nos proximos 4 anos se não me engano (e começar mais 4 até o fim desse ano).

## Sumário Projeto
#### Situação atual
- Dados:

Quando algum paciente vai até o seu cardiologista do convênio, as informações da consulta, com todo histórico de medicamentos já tomados, exames feitos e etc, estão armazenadas em um banco de dados X1 da clinica e alguns dados mais gerais são enviados para a seguradora para um banco de dados X2.

- Rotina do paciente:

Quando este mesmo paciente vai ao seu gastroenterologista, o mesmo processo ocorre mas agora estamos falando de um banco de dados X3 e a mesma lógica quando este paciente se interna em um hospital por alguma emergência.
Todos estes dados, em formatos múltiplos, prejudicam tanto para o médico como para a seguradora ter uma VISÃO LONGITUDINAL do paciente.
Logo temos uma dor para ser curada:
-Como ter uma visão mais COMPLETA e LONGITUDINAL do paciente ?

- Hospital:

Quando um hospital realiza um procedimento médico, uma cirurgia, receita antibióticos, ele solicita que o seguro médico pague por estes serviços; logo, o hospital tem a liberdade de fazer o que quiser com o paciente e o preço da internação fica cara quanto o hospital quiser: o hospital pode aumentar tempo de internação em UTI mesmo sem necessidade; pode optar por procedimentos desnecessários; pode escolher medicamentos mais caros, e etc. É o fee for service. A dor a ser curada é:
-Como usar outro modelo de pagamento para os serviços médicos, por exemplo um modelo pay for performance, afim de diminuir os gastos e proteger o paciente de uma prática médica enviesada?

#### Solução proposta
Uma forma de lidar com esses dois de muitos outros problemas que temos hoje no sistema de saúde suplementar é trabalhando com uma CDM (common data model); trazer todas essas informações, de diferentes fontes, para uma base de dados comum. Esta CDM já existe e se chama OMOP, é uma base com mais de 6 milhões de termos, so que é disponibilizada em inglês. Está é a empresa que gerencia o OMOP: https://www.ohdsi.org/ .

Existem duas empresas que fazem esse OMOP para o portugues até onde eu sei: https://www.janssen.com/brasil/ (da johnson & johnson) e também a https://www.iqvia.com/ . Pelo que foi comentado, a base do Albert Einstein é OMOP, mas o Gabriel comentou que era uma terceira empresa Americana que gerencia essa base que é em inglês mesmo (não sei muitos detalhes).

Além de permitir uma gestão mais eficiente por parte das seguradoras que terão métricas padronizadas de todo um HUB de serviços médicos de uma região, essa ferramenta pode ser interessante para os hospitais e clinicas também por propiciar uma clínica completa e padronizada, com mais dados , e um potencial de tratamento mais eficaz para os pacientes

## Estrutura de diretório

#### Camada Service
 Implementações de actions do Spark
 
#### Camada Business
 Implementação de transformations do Spark
 
#### Camada DAO
 Implementações de funções para interação com tabelas, arquivos e API[
 
#### Camada Object
 Objetos usados nas camadas superiores
 Abstração de tabelas e arquivos com objetos nativos da linguagem
 Exemplo:   Scala -> case class

## Arquitetura do projeto

![Alt text](./arquitetura_hc.png?raw=true "Title")


